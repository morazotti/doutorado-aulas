\documentclass[12pt, a4paper, brazil]{article}
\usepackage{color}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{slashed}
\usepackage[compat=1.1.0]{tikz-feynman} % Feynman diagrams
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\green}[1]{\textcolor{green}{#1}}
\newcommand{\tr}[1]{\mathrm{Tr}\left[#1\right]}
\newcommand{\dd}{\mathrm{d}}
\title{Espalhamento elétron-próton de altas ordens e aniquilação de
  pares elétron-pósitron}
\author{Nícolas André da Costa Morazotti}
\date{9 de Julho de 2019}

\begin{document}
{\let\newpage\relax\maketitle}  
\section{Correções de alta ordem de espalhamento elétron-próton}
\label{sec:correcoes-de-alta}
\begin{figure}
  \centering
  \subfloat{
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (a) {\(e^-\)};
        \vertex [above right=of a] (y);
        \vertex [above=of y] (x);
        \vertex [above left=of x] (b);
        \vertex [right=of y] (z);
        \vertex [below right=of z] (c);
        \vertex [above=of z] (w);
        \vertex [above right=of w] (d);
        \diagram*{
          (a) -- [fermion] (y) [vertex=\(y\)] -- [fermion] (x),
          (x) -- [fermion] (b),
          (c) -- [fermion, very thick] (z) -- [fermion, very thick] (w) --
          [fermion, very thick] (d),
          (y) -- [photon,edge label'=\(D_F(y-z)\)] (z),
          (x) -- [photon,edge label=\(D_F(x-w)\)] (w),
        }
      \end{feynman}
    \end{tikzpicture}
  }
  \subfloat{
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (a) {\(e^-\)};
        \vertex [above right=of a] (y);
        \vertex [above=of y] (x);
        \vertex [above left=of x] (b);
        \vertex [right=of y] (z);
        \vertex [below right=of z] (c);
        \vertex [above=of z] (w);
        \vertex [above right=of w] (d);
        \diagram*{
          (a) -- [fermion] (y) -- [fermion] (x),
          (x) -- [fermion] (b),
          (c) -- [fermion, very thick] (z) -- [fermion, very thick] (w) --
          [fermion, very thick] (d),
          (y) -- [photon] (w),
          (x) -- [photon] (z),
        }
      \end{feynman}
    \end{tikzpicture}
  }
  \caption{Contribuições para o espalhamento de elétron de quarta 
    ordem.}
  \label{fig:diagrama1}
\end{figure}

O espalhamento elétron-próton é calculado utilizando o processo
\begin{align}
  S_{fi}^{(n)}=-ie^n\int\int d^4y_1\cdots d^4y_n\bar{\psi}_f^{(+)}(y_n)\slashed
  A(y_n)S_F(y_n-y_{n-1})&\slashed A(y_{n-1}) \cdots\nonumber\\ 
          S_F(y_2-y_1)&\slashed A(y_1)\psi_i^{(+)}(y_1).\label{eq:1}
\end{align}
Na seção anterior, as contas foram até segunda ordem, $e^2$. Precisamos
então, para a próxima correção de maior ordem em $e$, utilizar a
\autoref{eq:1}, ficando com
\begin{align}
  S^{(2)}_{fi}=-ie^2\int d^4x d^4y\bar\psi_f(x)\slashed A(x)S_F(x-y)\slashed
  A(y)\psi_i(y)\label{eq:2}
\end{align}
onde o potencial eletromagnético é gerado pela corrente do próton. Para
determinar a forma da corrente em questão, olhamos à forma da corente do
elétron de segunda ordem interagindo com $A_\mu(x)$ e $A_\nu(y)$ na \autoref{eq:2}.

Podemos expandir a \autoref{eq:2} da forma
\begin{align*}
  S_{fi}^{(2)} &= -ie^2\int d^4 x d^4y \bar\psi_f(x)\gamma^\mu A_\mu(x)S_F(x-y)\gamma^\nu
                 A_\nu(y)\psi_i(y).
\end{align*}
Utilizando que, assim como no cálculo da primeira ordem, $S_{fi}^{(2)}$
deve ser simétrico em forma entre a corrente eletrônica e a corrente
protônica. A corrente (de Dirac) eletrônica é dada por
\begin{align*}
  i\bar\psi_f(x)\gamma^\mu S_F(x-y)\gamma^\nu \psi_i(y)=\bar\psi_f(x)\gamma^\mu\Bigg[
  &\sum_{n,p_0>0}\Theta(x_0-y_0)\psi_n(x)\bar\psi_n(y)\\
  -&\sum_{n,p_0<0}\Theta(y_0-x_0)\psi_n(x)
               \bar\psi_n(y)
               \Bigg]
               \gamma^\nu\psi_i(y).
\end{align*}

Para o campo $\slashed A(x)$, utilizamos as equações de Maxwell para a
corrente protônica:
\begin{align*}
  \square A_\mu(x)&=J_\mu(x)\\
          &=\int d^4y\delta^{(4)}(x-y)J_\mu(y).
\end{align*}
Contudo, utilizando a identidade $\square D_F(x-y)=\delta^{(4)}(x-y)$, podemos
concluir que 
\begin{align*}
  \square A_\mu(x)&=\int d^4y \square D_F(x-y)J_\mu(y)\\
          &=\square\int d^4y D_F(x-y)J_\mu(y),
\end{align*}
ou ainda,
\begin{align*}
  A_\mu(x)=\int d^4yD_F(x-y)J_\mu(y),
\end{align*}
Assim, o produto de $A_\mu(x)A_\nu(y)$ é
\begin{align*}
  A_\mu(x)A_\nu(y)=\int d^4w d^4zD_F(x-w)D_F(y-z)J_\mu(w)J_\nu(y),
\end{align*}
onde as correntes $J_\mu(w)$ e $J_\nu(z)$ também devem ser
simetrizadas. Podemos escrevê-las como
\begin{align*}
  J_\mu(w)J_\nu(z)=e^2_p\bar\psi^p_f(w)\gamma_\mu
  \Bigg [ &\sum_{n,p_0>0}\Theta(w_0-z_0)\psi^p_n(w)\bar\psi^p_n(z)\\
  -&\sum_{n,p_0<0}\Theta(z_0-w_0)\psi^p_n(w)\bar\psi^p_n(z)\Bigg]
     \gamma_\nu\psi^p_i(z)
\end{align*}
ficando então com
\begin{align}
  \label{eq:3}
  A_\mu(x)A_\nu(y)&=ie^2_p\int d^4wd^4zD_F(x-w)D_F(y-z)
                \bar\psi_f^p(w)\gamma_\mu S_F^p(w-z)\gamma_\nu\psi^p_i(z).
\end{align}
Contudo, devemos tomar cuidado: os fótons são indistinguíveis. Assim,
temos que computar ambos os diagramas apresentados na
\autoref{fig:diagrama1}. O primeiro diagrama já aparece acima com o
termo $D_F(x-w)D_F(y-z)$, que representa um fóton propagando de $x$ para
$w$ e outro de $y$ para $z$. Contudo, como são indistinguíveis, podemos
ter o caso oposto: um fóton se propaga de $x$ para $z$ e outro, de $y$
para $w$, com igual probabilidade. Então, somamos o termo
\begin{align*}
  ie_p^2\int d^4w d^4z D_F(x-z)D_F(y-w)\bar\psi_f(w)\gamma_\nu S_F^p(w-z)\psi^p_i(z).
\end{align*}
à \autoref{eq:3}.

Assim, a matriz de espalhamento de segunda ordem se torna
\begin{align}
  S_{fi}^{(2)}=e^2e_p^2 \int &d^4xd^4yd^4zd^4w\bar\psi^p_f(w)\gamma_\mu
                S_F(x-y)\gamma_\nu\psi_i(y)\nonumber\\
                          \{&D_F(x-w)D_F(y-z)\bar\psi^p_f(w)\gamma^\mu
                            S_F^p(w-z)\gamma^\nu\psi_i^p (z)\nonumber\\
  +&D_F(x-z)D_F(y-w)\bar\psi^p_f(w)\gamma^\nu S_F^p(w-z)\gamma^\mu\psi_i^p(z)\}.\label{eq:4}
\end{align}
% Sobre os fatores de $i$: queremos uma regra uniforme para tais. A regra
% que será utilizada será:{ Demonstrar tal regra.}

Perceba que nossas regras não são ainda claras para tratar os fatores de
$i$. Associamos um fator $(-i)$ à matriz $S$ e um fator $iS_F^p$ com o
propagador do \emph{próton}. Em mais altas ordens todos os propagadores
de próton $S_F^p$ serão acompanhados por um $i$ pelo mesmo motivo que
nosso presente exemplo. Podemos uniformizar a regra em relação a
propagadores fermiônicos se escrevermos também $iS_F$ para cada linha
eletrônica e associar um fator $-i$ com cada $\slashed A$:
\begin{align*}
  -ie\slashed AS_Fe\slashed AS_F\dots e\slashed A= (-ie\slashed
  A)iS_F(-ie\slashed A)\dots(-ie\slashed A).
\end{align*}
O fator global $(-i)$ é absorvido no fator extra de
$\slashed{A}$. Assim, para cada $\gamma_\mu$ na linha do elétron associamos um
fatorn $-i$. Em cada vértice do próton, associamos um $-i$ se
compensarmos ao colocar um $i$ na frente de cada propagador fotônico
$D_F$. Assim, temos uma regra uniforme: um $-i$ para cada vértice e um
$i$ para cada linha no diagrama.

Para fazer os cálculos, é útil transformar as integrações no espaço em
integrações no \emph{momentum}. Presumiremos que as funções de onda de
partículas externas (elétron e próton) sejam ondas planas. Os
propagadores, por sua vez, serão escritos como
\begin{align*}
  S_F(x-y) &= \int \frac {d^4p}{(2\pi)^4}
             \sqrt{\frac{m^2}{E_fE_i}} e^{-ip\cdot(x-y)}
             \frac1{\slashed p-m+i\epsilon}\nonumber\\ 
  S_F^p(z-w) &= \int \frac {d^4P}{(2\pi)^4}
               \sqrt{\frac{M^2}{E^p_fE^p_i}} e^{-iP\cdot(z-w)}
               \frac1{\slashed P-M+i\epsilon}\nonumber\\
  D_F(x-w) &= \int \frac {d^4 q_1}{(2\pi)^4}\frac{e^{-iq_1\cdot(x-w)}}{q_1^2+i\epsilon}.
\end{align*}
Assim, o primeiro dos termos na \autoref{eq:4} é
\begin{align}
  &\frac{e^4}{V^2}\int d^4x\ d^4y\ d^4z\ d^4w
    \sqrt{\frac{m^2}{E_fE_i}}\sqrt{\frac{M^2}{E^p_fE^p_i}}
    \frac{d^4q_1}{(2\pi)^4}
    \frac{d^4q_2}{(2\pi)^4} \frac{d^4p}{(2\pi)^4}
    \frac{d^4P}{(2\pi)^4}\nonumber\\
  \times&\frac{e^{-iq_1\cdot(x-w)}}{q_1^2+i\epsilon}
    \frac{e^{-iq_2\cdot(y-z)}}{q_2^2+i\epsilon}
    \left[e^{ip_f\cdot x}\bar u(p_f,s_f)\gamma_\mu
    \frac{e^{-ip\cdot(x-y)}}{\slashed p-m+i\epsilon}
    \gamma_\nu u(p_i,s_i)e^{-ip_i\cdot y}
     \right]\nonumber\\
  \times&\left[e^{iP_f\cdot w}\bar u(P_f,S_f)\gamma^\mu
    \frac{e^{-iP\cdot(w-z)}}{\slashed P-M+i\epsilon}
    \gamma^\nu u(P_i,S_i)e^{-iP_i\cdot z}\right].\label{eq:5}
\end{align}
Veja que podemos realizar as integrações nas posições, reorganizando as
exponenciais:
\begin{align*}
  \int d^4x\ d^4y\ d^4z\ d^4w\ &e^{-iq_1\cdot(x-w)}e^{-iq_2\cdot(y-z)}e^{ip_f\cdot x}
                              e^{-ip\cdot(x-y)}e^{-ip_i\cdot y}\\
                            &e^{iP_f\cdot w}e^{-iP\cdot(w-z)}e^{-iP_i\cdot z}\\
  =\int d^4x\ d^4y\ d^4z\ d^4w\ & e^{i(p_f-q_1-p)\cdot x} e^{i(p-q_2-p_i)\cdot y}
                                 e^{i(q_2+P-P_i)\cdot z}e^{i(P_f-P+q_1)\cdot w}\\
  =[(2\pi)^4]^4 \delta^{(4)}(p_f-q_1-p) &\delta^{(4)}(p-q_2-p_i) \delta^{(4)}(q_2+P-P_i)
               \delta^{(4)}(P_f-P+q_1).
\end{align*}
Aqui, temos as deltas que garantem a conservação de
energia-\emph{momentum} em cada vértice associado a cada
coordenada. Integrando sobre $q_2$, $p$ e $P$, resta apenas a delta
$\delta^{(4)}(P_f+p_f-P_i-p_i)$, com as condições
\begin{align*}
  q_2 &= p_f-q_1-p_i\\
  p &= p_f -q_1\\
  P &= P_f + q_1.
\end{align*}
Definimos $q\equiv p_f-p_i$. Assim, $q_2 = q-q_1$ e a \autoref{eq:5} se torna
\begin{align*}
 &\frac{e^4}{V^2}
   \sqrt{\frac{m^2}{E_fE_i}}\sqrt{\frac{M^2}{E^p_fE^p_i}}
   \delta^{(4)}(P_f+p_f-P_i-p_i)
   \int d^4q_1\frac{1}{q_1^2+i\epsilon}
    \frac{1}{(q-q_1)^2+i\epsilon}\nonumber\\
  \times&\left[\bar u(p_f,s_f)\gamma_\mu
    \frac{1}{\slashed p_f - \slashed q_1 -m+i\epsilon}
    \gamma_\nu u(p_i,s_i)
     \right]\nonumber\\
  \times&\left[\bar u(P_f,S_f)\gamma^\mu
    \frac{1}{\slashed P_f +\slashed q_1-M+i\epsilon}
    \gamma^\nu u(P_i,S_i)\right]  .
\end{align*}

O segundo dos termos da \autoref{eq:4} pode ser calculado
também. Ele é escrito como
\begin{align}
    &\frac{e^4}{V^2}\int d^4x\ d^4y\ d^4z\ d^4w
    \sqrt{\frac{m^2}{E_fE_i}}\sqrt{\frac{M^2}{E^p_fE^p_i}}
    \frac{d^4q_1}{(2\pi)^4}
    \frac{d^4q_2}{(2\pi)^4} \frac{d^4p}{(2\pi)^4}
    \frac{d^4P}{(2\pi)^4}\nonumber\\
  \times&\frac{e^{-iq_1\cdot(x-z)}}{q_1^2+i\epsilon}
    \frac{e^{-iq_2\cdot(y-w)}}{q_2^2+i\epsilon}
    \left[e^{ip_f\cdot x}\bar u(p_f,s_f)\gamma_\mu
    \frac{e^{-ip\cdot(x-y)}}{\slashed p-m+i\epsilon}
    \gamma_\nu u(p_i,s_i)e^{-ip_i\cdot y}
     \right]\nonumber\\
  \times&\left[e^{iP_f\cdot w}\bar u(P_f,S_f)\gamma^\mu
    \frac{e^{-iP\cdot(z-w)}}{\slashed P-M+i\epsilon}
    \gamma^\nu u(P_i,S_i)e^{-iP_i\cdot z}\right].\label{eq:6}.
\end{align}
A integração sobre as posições nos dá
\begin{align*}
  [(2\pi)]^4 \delta^{(4)}(p_f-p_q_1)\delt a^{(4)}(p-p_i-q_2)\delt
  a^{(4)}(q_2+P_f+P)\delt a^{(4)}(q_1-P-P_i), 
\end{align*}
cuja integração nos \emph{momenta} $q_2$, $p$ e $P$ resulta em
\begin{align*}
  &\frac{e^4}{V^2} 
    \sqrt{\frac{m^2}{E_fE_i}}\sqrt{\frac{M^2}{E^p_fE^p_i}}
    \delta^{(4)}(P_f+p_f-P_i-p_i)
    \int d^4q_1 \frac{1}{q_1^2+i\epsilon}
    \frac{1}{(q-q_1)^2+i\epsilon}\nonumber\\
  \times& \left[\bar u(p_f,s_f)\gamma_\mu
    \frac{1}{\slashed p_f -\slashed q_1 -m+i\epsilon}
    \gamma_\nu u(p_i,s_i)
     \right]\nonumber\\
  \times&\left[\bar u(P_f,S_f)\gamma^\mu
    \frac{1}{\slashed q_1 - \slashed P_i -M+i\epsilon}
    \gamma^\nu u(P_i,S_i)\right].
\end{align*}

\newpage
\section{Aniquilação de pares em raios $\gamma$}
\label{sec:aniquilacao-de-pares}
Se girarmos de $90^\circ$ os diagramas para o espalhamento Compton, teremos
um processo físico interessante. Entendemos tal processo como a
aniquilação de um par elétron-pósitron em dois fótons. A
\autoref{fig:aniquilacao-de-pares} apresenta os dois diagramas
relacionados com a aniquilação. Ambos os diagramas são importantes uma
vez que os fótons são indistinguíveis.
\begin{figure}
  \centering
  \subfloat{
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (a) {\(e^-\)};
        \vertex [above right=of a] (x);
        \vertex [above left=of x] (f1) {\(k_1,\epsilon_1\)};
        \vertex [right=of x] (y);
        \vertex [above right=of y] (f2) {\(k_2,\epsilon_2\)};
        \vertex [below right=of y] (b);

        \diagram*{ (a) -- [fermion,momentum=\(p_-\)] (x) --
          [fermion,,momentum'=\(p_--k_1\)] (y) -- [fermion,momentum=\(-p_+\)] (b),
          (x) -- [photon] (f1),
          (y) -- [photon] (f2),
        }
      \end{feynman}
    \end{tikzpicture}
  }
  \subfloat{
    \begin{tikzpicture}
      \begin{feynman}
        \vertex (a) {\(e^-\)};
        \vertex [above right=of a] (x);
        \vertex [above left=of x] (f1) {\(k_1,\epsilon_1\)};
        \vertex [right=of x] (y);
        \vertex [above right=of y] (f2) {\(k_2,\epsilon_2\)};
        \vertex [below right=of y] (b);

        \diagram*{ (a) -- [fermion,momentum=\(p_-\)] (x) --
          [fermion,momentum'=\(p_--k_2\)] (y) -- [fermion,momentum=\(-p_+\)] (b), 
          (x) -- [photon] (f2),
          (y) -- [photon] (f1),
        }  
      \end{feynman}
    \end{tikzpicture}
    \caption{Diagramas de aniquilação de pares elétron-pósitron.}
    \label{fig:aniquilacao-de-pares}
  }
\end{figure}
Calculemos o elemento da matriz $S$ referente aos diagramas: a interação
é feita com um campo $\slashed{A}$ livre, e não sabemos de qual posição
sai cada fóton. Então
\begin{align*}
  S_{fi}^{(pair)}=\int d^4xd^4y \bar\psi_+(y)\Big[&
    (-ie \slashed{A}(y,k_1))
    iS_F(x-y)
    (-ie\slashed{A}(x,k_2))\\
  +&(-ie \slashed{A}(y,k_2))
                   iS_F(x-y)
                   (-ie\slashed{A}(x,k_1))
                   \Big]
                   \psi_-(x).
\end{align*}
A função de onda $\bar\psi_+(y)$ é de um \emph{pósitron} e, assim, é
representada pelos spinores $v$. Substituímos
\begin{align*}
  \bar\psi_+(y)&=\sqrt{\frac{m}{E_+}}e^{-ip_+\cdot y}\bar v(-p_+,s_+),\\
  \psi_-(x)&=\sqrt{\frac{m}{E_-}}e^{-ip_-\cdot x} u(p_-,s_-)\\
  A_\mu(x,k_i)&=\frac1{\sqrt{2k_iV}}\epsilon_i_\mu(e^{-ik_i\cdot x}+e^{ik_i\cdot x})\\
  S_F(x-y)&= \int\frac{d^4q}{(2\pi)^4}\frac1{\slashed{q}-m}e^{-iq\cdot(x-y)}
\end{align*}
e chegamos a
\begin{align*}
  S_{fi}^{(pair)}
  &=-\frac{ie^2}V\int d^4xd^4y\frac{d^4q}{(2\pi)^4}\sqrt{\frac{m^2}{2k_12k_2E_+E_-}}
    \bar v(-p_+,s_+)e^{-ip_+\cdot y}e^{-ip_-\cdot x}e^{-iq\cdot(x-y)}\gamma^\mu\\
  &\Bigg[\epsilon_1_\mu\frac1{\slashed{q}-m}\epsilon_2_\mu(e^{-ik_1\cdot y}+e^{ik_1\cdot y})
    (e^{-ik_2\cdot x}+e^{ik_2\cdot x})\\
  &+\epsilon_2_\mu\frac1{\slashed{q}-m}\epsilon_1_\mu(e^{-ik_2\cdot y}+e^{ik_2\cdot y})
    (e^{-ik_1\cdot x}+e^{ik_1\cdot x})
    \Bigg]\gamma^\nu u(p_-,s_-)\\
  &=-\frac{ie^2}V\int d^4xd^4y\frac{d^4q}{(2\pi)^4}\sqrt{\frac{m^2}{2k_12k_2E_+E_-}}
    \bar v(-p_+,s_+)e^{-ip_+\cdot y}e^{-ip_-\cdot x}e^{-iq\cdot(x-y)}\gamma^\mu\\
  &\Bigg[\epsilon_1_\mu\frac1{\slashed{q}-m}\epsilon_2_\mu(
    e^{-ik_2\cdot x} e^{-ik_1\cdot y}
    +e^{-ik_2\cdot x}e^{ik_1\cdot y}
    +e^{ik_2\cdot x}e^{-ik_1\cdot y}
    +e^{ik_2\cdot x}e^{ik_1\cdot y}) \\
  &+\epsilon_2_\mu\frac1{\slashed{q}-m}\epsilon_1_\mu
    (e^{-ik_2\cdot y}e^{-ik_1\cdot x}
    +e^{ik_2\cdot y} e^{-ik_1\cdot x}
    +e^{-ik_2\cdot y}e^{ik_1\cdot x}
    +e^{ik_2\cdot y}e^{ik_1\cdot x})
    \Bigg]\gamma^\nu u(p_-,s_-).
\end{align*}
Podemos unir as exponenciais em dois expoentes, $x$ e $y$, e integrar
sobre as posições. No termo com $\epsilon_1_\mu\frac1{\slashed{q}-m}\epsilon_2_\mu$,
\begin{align*}
  \int d^4xd^4y[
   &e^{ix\cdot(-q-p_- -k_2)}e^{iy\cdot(q-p_+-k_1)}
   +e^{ix\cdot(-q-p_- -k_2)}e^{iy\cdot(q-p_++k_1)}\\
  +&e^{ix\cdot(-q-p_- +k_2)}e^{iy\cdot(q-p_+-k_1)}
   +e^{ix\cdot(-q-p_- +k_2)}e^{iy\cdot(q-p_++k_1)}]\\
   &=(2\pi)^8\Bigg[
      \delta^4(-q-p_- -k_2)\delta^4(q-p_+-k_1)
     +\delta^4(-q-p_- -k_2)\delta^4(q-p_++k_1)\\
    &+\delta^4(-q-p_- +k_2)\delta^4(q-p_+-k_1)
     +\delta^4(-q-p_- +k_2)\delta^4(q-p_++k_1)
     \Bigg].
\end{align*}
Quando integrarmos no \emph{momentum}, por culpa da estrutura do
diagrama e da conservação do $4-$\emph{momentum}, apenas as deltas
$\delta^4(-q-p_- +k_2)\delta^4(q-p_++k_1)$ restam. Ao integrar sobre $q$, resta
apenas uma delta $\delta^4(k_1+k_2-p_+-p_-)$.

Ao realizarmos o mesmo processo para o outro termo, descobrimos que o
elemento da matriz S referente aos diagramas da \autoref{fig:aniquilacao-de-pares} é
\begin{align}
  S_{fi}^{(pair)}&=\frac{e^2}{V^2}\sqrt{\frac{m^2}{E_+E_-2k_12k_2}}
                   (2\pi)^4\delta^{(4)}(k_1+k_2-p_+-p_-)\bar
                   v(p_+,s_+)\nonumber\\
                 &\times\left[(-i\slashed\epsilon_2)\frac{i}{\slashed p_- -\slashed
                   k_1-m}(-i\slashed\epsilon_1)+(-i\slashed\epsilon_1)\frac{i}{\slashed
                   p_- -\slashed k_2-m}(-i\slashed\epsilon_2)\right]u(p_-,s_-).
                   \label{eq:7}
\end{align}
Tal elemento de matriz pode ser obtido utilizando as já vistas regras, e
seguindo fielmente a demonstração do espalhamento Compton. Veja, podemos
interpretar o elétron saindo como um elétron propagando-se para trás no
tempo, com uma energia negativa $-p_+$. Uma vez que a aniquilação de
pares para um foton único não conserva energia nem \emph{momentum}, esta
é a menor ordem possível em que tal processo pode ocorrer.

Olhando a amplitude de espalhamento Compton, vemos uma grande
similaridade entre as matrizes de espalhamento. Podemos definir uma
regra que transforma uma amplitude na outra:
\begin{align*}
  \epsilon,k&\leftrightarrow\epsilon_1,-k_1\\
  \epsilon',k'&\leftrightarrow\epsilon_2,+k_2\\
  p_i,s_i&\leftrightarrow p_-,s_-\\
  p_f,s_f&\leftrightarrow -p_+,+s_+.
\end{align*}
Em verdade, tal regra vale sempre que relacionarmos processos da forma
\begin{align*}
  A+B\to C+D
\end{align*}
e
\begin{align*}
  A+\bar C\to\bar B+D,
\end{align*}
onde a barra representa antipartícula ($\bar B$ é antipartícula de $B$,
por exemplo).

Para calcular a seção de choque, fazemos o usual: tomaremos o módulo
quadrado da \autoref{eq:7} e dividiremos por $(2\pi)^4\delta{(0)}$ para termos
uma taxa (regra de ouro). Tão logo isso feito, dividiremos pelo fluxo
incidente $|\mathbf{v}|/V$ (onde $|\mathbf{v}|$ é o módulo da velocidade
do pósitron incidente, que podemos escrever também como $p_+/E_+$) e
pelo número de partículas por unidade de volume $1/V$, e integraremos
por fim por todo o espaço de fase $[V^2/(2\pi)^6]d^k_1d^3k_2$ (aqui, são
os \emph{momenta} finais, afinal queremos a seção de choque depois da
aniquilação). Além disso, consideremos o elétron não polarizado em
repouso no referencial do laboratório, tornando então $E_-=m$.
\begin{align}
  |S_{fi}^{(pair)}|^2 &= \frac{e^4}{V^4}
                        \frac{m^2}{4p_+mk_1k_2}(2\pi)^8
                        \delta^{(4)}(k_1+k_2-p_+-p_-)
                        \delta^{(4)}(k_1+k_2-p_+-p_-)\nonumber\\
                      &\times \left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
                        \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
                        \slashed\epsilon_1
                        \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
                        \right)u(p_-,s_-)\right|^2.\label{eq:8}
\end{align}
Como a expressão tem duas da mesma delta, então independentemente de seu
argumento, podemos alterar uma destas para $\delta^{(4)}(0)$ pois, caso uma
delas tenha argumento nulo, a outra também terá, e caso o argumento seja
não nulo, então o da outra também não será. Já que a expressão é nula
para o argumento não nulo, e diferente de $0$ para o argumento nulo,
então podemos fazer a substituição. Dividindo então a 
\autoref{eq:8} por $(2\pi)^4\delta{(0)}$, temos
\begin{align*}
  \frac{|S_{fi}^{(pair)}|^2}{(2\pi)^4\delta{(0)}}
  &= \frac{e^4}{V^4}
    \frac{m}{4p_+k_1k_2}(2\pi)^4
    \delta^{(4)}(k_1+k_2-p_+-p_-)\nonumber\\
  &\times \left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2.
\end{align*}
Veja que, ao dividir por $1/V^2$ e multiplicar por
$[V^2/(2\pi)^6]d^3k_1d^3k_2$ cancela todos os fatores de $V$
envolvidos. Então, a seção de choque $d\sigma$ se torna
\begin{align*}
  d\sigma &= \int\frac{V^4}{(2\pi)^6}
       \frac{|S_{fi}^{(pair)}|^2}{(2\pi)^4\delta{(0)}}
       d^3k_1d^3k_2\nonumber\\
     &=\frac{e^4m}{(2\pi)^2p_+} \int\frac{d^3k_1}{2k_1}
       \frac{d^3k_2}{2k_2}\delta^{(4)}(k_1+k_2-p_+-p_-)\nonumber\\
     &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2.
\end{align*}
Podemos utilizar a identidade
\begin{align*}
  \frac{d^3p}{2E}&=\int_0^\infty dp_0\delta(p_\mu p^\mu -m^2)d^3p
                   =\int_\infty^\infty d^4p\delta(p_\mu p^\mu -m^2)\theta(p_0)
\end{align*}
para reescrever, por exemplo, a parte em $k_2$:
\begin{align*}
  d\sigma&=\frac{e^4m}{(2\pi)^2p_+} \int\frac{d^3k_1}{2k_1}
      {d^4k_2}\delta(k_2_\mu k_2^\mu)\delta^{(4)}(k_1+k_2-p_+-p_-)\theta(k_2)\nonumber\\
     &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2
\end{align*}
e integrar $\delta^{(4)}(k_1+k_2-p_+-p_-)$ em $k_2$:
\begin{align*}
  d\sigma&=\frac{e^4m}{(2\pi)^2p_+} \int\frac{d^3k_1}{2k_1}
      \delta((p_++p_--k_1)_\mu (p_++p_--k_1)^\mu)\theta(p_++p_--k_1)\nonumber\\
     &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2.
\end{align*}
Considerando um ângulo $\theta$ ao redor de $k_1$, e colocando a integral de
$k_1$ no ângulo sólido para fora, podemos achar a seção de choque
diferencial
\begin{align*}
  \frac{d\sigma}{d\Omega_{k_1}}
  &=\frac{e^4m}{(2\pi)^2p_+} \int_0^\infty\frac{k_1dk_1}{2}
    \delta((p_++p_--k_1)_\mu (p_++p_--k_1)^\mu)\theta(p_+_0+p_-_0-k_1_0)\nonumber\\
  &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2.\nonumber\\
  &=\frac{e^4m}{(2\pi)^2p_+} \int_0^{E_++m}\frac{k_1dk_1}{2}
    \delta((p_++p_--k_1)_\mu (p_++p_--k_1)^\mu)\nonumber\\
  &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2.
\end{align*}
Agora, precisamos tratar da delta. Podemos expandir o produto escalar
como
\begin{align*}
(p_++p_--k_1)_\mu (p_++p_--k_1)^\mu&=p_+_\mu p_+^\mu+p_-_\mu p_-^\mu-k_1_\mu k_1^ \mu\\
                             &+2p_+_\mu p_-^\mu-2p_+_\mu k_1^\mu-2p_-_\mu k_1^\mu.
\end{align*}
Nos termos quadrados, $p_-^\mu p_-^\mu = m^2=p_+^\mu p_+^\mu$, e $k_1_\mu
k_1^\mu=0$. Nos termos cruzados, precisamos expandir a soma sobre $\mu$, e
lembrar que nosso referencial tem o elétron em repouso, tal que
$\vec{p_i} = 0$:
\begin{align*}
  p_+_\mu p_-^\mu &= p_+_0 p_-^0 - \vec{p_+}\cdot\vec{p_-}\\
              &= E_++m\\
  p_+_\mu k_1^\mu &= p_+_0 k_1^0 - \vec{p_+}\cdot\vec{k_1}\\
              &= E_+ k_1  - \vec{p_+}\cdot\vec{k_1}\\
  p_-_\mu k_1^\mu &= p_-_0 k_1^0 - \vec{p_-}\cdot\vec{k_1}\\
              &= mk_1.
\end{align*}
Assim:
\begin{align*}
  (p_++p_--k_1)_\mu (p_++p_--k_1)^\mu
  &= 2m^2 + 2(E_++m)-2(E_++m)k_1+2\vec{p_+}\cdot\vec{k_1}.
\end{align*}
Escrevemos o termo de produto escalar $\vec{p_+}\cdot\vec{k_1}$:
\begin{align*}
  \vec{p_+}\cdot\vec{k_1}&=p_+k_1\cos\theta.
\end{align*}
Assim, a seção de choque diferencial se torna
\begin{align}
  \frac{d\sigma}{d\Omega_{k_1}}
  &=\frac{e^4m}{(2\pi)^2p_+} \int_0^{E_++m}\frac{k_1dk_1}{2}
    \delta[2m^2+2mE_+-2k_1(m+E_+-p_+\cos\theta)]\nonumber\\
  &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2.\label{eq:10}
\end{align}
Podemos utilizar uma identidade da delta, ficando então com
\begin{align*}
  \frac{d\sigma}{d\Omega_{k_1}}
  &=\frac{e^4m}{(2\pi)^2p_+}\frac1{2(m+E_+-p_+\cos \theta)}
    \int_0^{E_++m}\frac{k_1dk_1}{2}
    \delta\left(k_1-\frac{m^2+E_+m}{m+E_+-p_+\cos\theta}\right)\nonumber\\
  &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2  \\
  &=\frac{e^4m}{(2\pi)^2p_+}\frac{(m^2+E_+m)}{4(m+E_+-p_+\cos \theta)^2}
    \nonumber\\
  &\times\left|\bar v(p_+,s_+)\left(\slashed\epsilon_2
    \frac1{\slashed p_--\slashed k_1-m}\slashed\epsilon_1 +
    \slashed\epsilon_1
    \frac1{\slashed p_--\slashed k_2-m}\slashed\epsilon_2
    \right)u(p_-,s_-)\right|^2  .
\end{align*}

Agora que resolvemos a integral, precisamos tratar do \emph{spinor}. No
fim das contas substituiremos $k_1=\frac{m^2+E_+m}{m+E_+-p_+\cos\theta}$ e
$k_2=p_++p_--k_1$. Dentro do módulo quadrado, temos
\begin{align}
  \bar v(p_+,s_+)\left(\slashed\epsilon_2
  \frac{\slashed p_--\slashed k_1 + m}
  {(p_--k_1)^2-m^2}\slashed\epsilon_1
  +
  \slashed\epsilon_1\frac{\slashed p_--\slashed k_2 + m}
  {(p_--k_2)^2-m^2}\slashed\epsilon_2\right)u(p_-,s_-).\label{eq:9}
\end{align}
Escolhamos aqui um \emph{gauge} especial, tal que as polarizações dos
fótons sejam $$\epsilon_i^\mu=(0,\vec\varepsilon_i),$$ tal que $$\vec\varepsilon_i\cdot\vec k_i = 0.$$
Assim, $\epsilon_i_\mu p_-^\mu =0$ pois escolhemos o elétron em repouso no
referencial do laboratório.

Veja, primeiramente, como $\slashed p_-\slashed\epsilon_i$ se comporta:
\begin{align*}
  \slashed p_-\slashed\epsilon_i
  &= \underbrace{p_-_\mu\epsilon_i^\mu}_{\equiv0} - i\sigma^{\mu\nu}p_-_\mu\epsilon_i_\nu\\
  &= i\sigma^{\nu\mu}\epsilon_i_\nu p_-_\mu\\
  &= -\slashed\epsilon_i\slashed p_-.
\end{align*}
Então, o termo $(\slashed p_-+m)\slashed\epsilon_iu=\slashed\epsilon_i(-\slashed
p_-+m)u$. Esse termo, a menos de uma constante, é o projetor para
energias \emph{negativas}, e $\Lambda_-u\equiv0.$ Podemos simplificar o denominador
ao perceber que
\begin{align*}
  (p_--k_i)^2-m^2
  &= p_-_\mu p_-^\mu -2k_i\cdot p_-+k_i_\mu k_i^\mu-m^2\\
  &= m^2 - 2k_i\cdot p_--m^2\\
  &= - 2k_i\cdot p_-.
\end{align*}
Assim, reescrevemos o termo
\begin{align*} 
  &\bar v(p_+,s_+)\left(
  \frac{\slashed\epsilon_2\slashed k_1\slashed\epsilon_1} {2k_1\cdot p_-}
  + \frac{\slashed\epsilon_1\slashed k_2\slashed\epsilon_2}{2k_2\cdot p_-}
  \right)u(p_-,s_- )\\
  = -&\bar v(p_+,s_+)\left(
  \frac{\slashed\epsilon_2\slashed\epsilon_1\slashed k_1} {2k_1\cdot p_-}
  + \frac{\slashed\epsilon_1\slashed\epsilon_2\slashed k_2}{2k_2\cdot p_-}
  \right)u(p_-,s_- ),
\end{align*}
o colocamos dentro da \autoref{eq:10} e realizamos a média sobre os
\emph{spins} do elétron e do pósitron:
\begin{align}
  \frac{d\bar\sigma}{d\Omega_{k_1}}
  &=\frac14\sum_{s_-,s_+}
    \frac{e^4m^2(m+E_+)}{4(2\pi)^2p_+(m+E_+-p_+\cos\theta)^2}\\  
  &\times \left|\bar v(p_+,s_+)\left(
    \frac{\slashed\epsilon_2\slashed\epsilon_1\slashed k_1} {2k_1\cdot p_-}
    + \frac{\slashed\epsilon_1\slashed\epsilon_2\slashed k_2}{2k_2\cdot p_-}
    \right)u(p_-,s_- )\right|^2.\label{eq:12}
\end{align}

Vamos utilizar a expressão (7.13) do livro:
\begin{align*}
  |\bar u(f)\Gamma u(i)|^2=|\bar u(f)\Gamma u(i)||\bar u(i)\bar\Gamma u(f)|,
\end{align*}
com $\bar \Gamma=\gamma^0\Gamma\gamma^0$. Seja, então,
\begin{align*}
  \Gamma&=\frac{\slashed\epsilon_2\slashed\epsilon_1\slashed k_1}{2k_1\cdot p_-} +
   \frac{\slashed\epsilon_1\slashed\epsilon_2\slashed k_2}{2k_2\cdot p_-},\\
  u(f)&=v(p_+,s_+),\\ 
  u(i)&=u(p_-,s_-).
\end{align*}
Podemos reescrever a soma
\begin{align*}
  \sum_{s_-,s_+}\bar v(p_+,s_+)\Gamma u(p_-,s_-)\bar u(p_-,s_-)\bar\Gamma v(p_+,s_+)
\end{align*}
como
\begin{align*}
  \sum_{\alpha,\beta,\gamma,\delta}\sum_{s_-,s_+} \bar v_\alpha(p_+,s_+) \Gamma_{\alpha\beta} u_\beta(p_-,s_-)\bar
  u_\gamma(p_-,s_-)\bar\Gamma_{\gamma\delta} v_\delta(p_+,s_+) 
\end{align*}
e usar a identidade
\begin{align*}
  \sum_{s_-}u_\beta(p_-,s_-)\bar u_\gamma(p_-,s_-)
  &\equiv\left(\frac{\slashed p_-+m}{2m}\right)_{\beta\gamma}
\end{align*}
para atingir
\begin{align*}
  \sum_{\alpha,\beta,\gamma,\delta}\sum_{s_-,s_+} \bar v_\alpha(p_+,s_+) \Gamma_{\alpha\beta}
  \left(\frac{\slashed p_-+m}{2m}\right)_{\beta\gamma}
  \bar\Gamma_{\gamma\delta} v_\delta(p_+,s_+).
\end{align*}
Além disso, veja que o termo
\begin{align*}
  \sum_{s_+}v_\delta(p_+,s_+)\bar v_\alpha(p_+,s_+)
  &=-\left(\frac{m-\slashed p_+}{2m}\right)_{\delta\alpha},
\end{align*}
restando então
\begin{align}
  &\sum_{\alpha,\beta,\gamma,\delta}\left(\frac{m-\slashed p_+}{2m}\right)_{\delta\alpha}\Gamma_{\alpha\beta}
  \left(\frac{\slashed p_-+m}{2m}\right)_{\beta\gamma}
    \bar\Gamma_{\gamma\delta} \nonumber \\
  =&\tr{\left(\frac{m-\slashed p_+}{2m}\right)\Gamma
  \left(\frac{\slashed p_-+m}{2m}\right)
    \bar\Gamma}\nonumber\\
  =&-\tr{\left(\frac{m-\slashed p_+}{2m}\right)
     \left(\frac{\slashed\epsilon_2\slashed\epsilon_1\slashed k_1}{2k_1\cdot p_-} +
     \frac{\slashed\epsilon_1\slashed\epsilon_2\slashed k_2}{2k_2\cdot p_-}\right)
     \left(\frac{\slashed p_-+m}{2m}\right)
     \left(\frac{\slashed k_1\slashed\epsilon_1\slashed\epsilon_2}{2k_1\cdot p_-} +
     \frac{\slashed k_2\slashed\epsilon_2\slashed\epsilon_1}{2k_2\cdot
     p_-}\right)}.\label{eq:11}
\end{align}
Juntando então a \autoref{eq:11} à \autoref{eq:12}, e usando que
$\alpha=e^2/4\pi$, 
\begin{align*}
  \frac{d\bar\sigma}{d\Omega_{k_1}}
  &=-\frac14
    \frac{e^4m^2(m+E_+)}{4(2\pi)^2p_+(m+E_+-p_+\cos\theta)^2}\\
  &\times\tr{\left(\frac{m-\slashed p_+}{2m}\right)
     \left(\frac{\slashed\epsilon_2\slashed\epsilon_1\slashed k_1}{2k_1\cdot p_-} +
     \frac{\slashed\epsilon_1\slashed\epsilon_2\slashed k_2}{2k_2\cdot p_-}\right)
     \left(\frac{\slashed p_-+m}{2m}\right)
     \left(\frac{\slashed k_1\slashed\epsilon_1\slashed\epsilon_2}{2k_1\cdot p_-} +
     \frac{\slashed k_2\slashed\epsilon_2\slashed\epsilon_1}{2k_2\cdot
    p_-}\right)}\\
  &=-\frac1{16}
    \frac{\alpha^2(m+E_+)}{p_+(m+E_+-p_+\cos\theta)^2}\\
  &\times \tr{\left({m-\slashed p_+}\right)
     \left(\frac{\slashed\epsilon_2\slashed\epsilon_1\slashed k_1}{2k_1\cdot p_-} +
     \frac{\slashed\epsilon_1\slashed\epsilon_2\slashed k_2}{2k_2\cdot p_-}\right)
     \left({\slashed p_-+m}\right)
     \left(\frac{\slashed k_1\slashed\epsilon_1\slashed\epsilon_2}{2k_1\cdot p_-} +
     \frac{\slashed k_2\slashed\epsilon_2\slashed\epsilon_1}{2k_2\cdot
    p_-}\right)}.
\end{align*}
Os termos de traço de um número ímpar de matrizes $\gamma$ são nulos.
O traço de seis elementos pode ser calculado utilizando
\begin{align*}
  \tr{\slashed{a}\slashed b} &= 4 a\cdot b\\
  \tr{\slashed{a}\slashed{b} \slashed{c} \slashed{d}}
                             &= a\cdot b\tr{\slashed{c}\slashed{d}}
                               -a\cdot c\tr{\slashed{b} \slashed{d}}
                               +a\cdot d\tr{\slashed{b}\slashed{c}}\\
                             &=a\cdot b(4c\cdot d)
                               -a\cdot c(4b\cdot d)
                               +a\cdot d(4b\cdot c)\\
  \tr{\slashed{a}\slashed{b}\slashed{c}\slashed{d}\slashed{e}\slashed{f}}
                            &= (a\cdot b)\tr{\slashed{c}\slashed{d}\slashed{e}\slashed{f}}\\
                             &-(a\cdot c)\tr{\slashed{b}\slashed{d}\slashed{e}\slashed{f}}\\
                             &+(a\cdot d)\tr{\slashed{b}\slashed{c}\slashed{e}\slashed{f}}\\
                             &-(a\cdot e)\tr{\slashed{b}\slashed{c}\slashed{d}\slashed{f}}\\
                             &+(a\cdot f)\tr{\slashed{b}\slashed{c}\slashed{d}\slashed{e}}
\end{align*}
Veja que os termos diretos do traço ($m^2$ com os elementos de mesmo
índice) são nulos:
\begin{align*}
  \tr{\slashed{\epsilon}_{3-i} \slashed{\epsilon}_i \slashed{k}_i \slashed{k}_i
  \slashed{\epsilon}_i \slashed{\epsilon}_{3-i}}
  &= 4(\epsilon_{3-i}\cdot \epsilon_i)
    [(k_i\cdot k_i)(\epsilon_i\cdot\epsilon_{3-i})-(k_i\cdot\epsilon_i)(k_i\cdot\epsilon_{3-i})+(k_i\cdot\epsilon_{3-i})(k_i\cdot\epsilon_i)]\\
  &-4(\epsilon_{3-i}\cdot k_i)
    [(\epsilon_i\cdot k_i)(\epsilon_i\cdot \epsilon_{3-i})-(\epsilon_i\cdot \epsilon_i)(k_i\cdot\epsilon_{3-i} )+(\epsilon_i\cdot\epsilon_{3-i} )(k_i\cdot\epsilon_i )]\\
  &+4(\epsilon_{3-i}\cdot k_i)
    [(\epsilon_i\cdot k_i)(\epsilon_i\cdot\epsilon_{3-i} )-(\epsilon_i\cdot\epsilon_i )(k_i\cdot \epsilon_{3-i} )+(\epsilon_i\cdot\epsilon_{3-i} )(k_i\cdot\epsilon_i )]\\
  &-4(\epsilon_{3-i}\cdot \epsilon_i)
    [(\epsilon_i\cdot k_i)(k_i\cdot\epsilon_{3-i} )-(\epsilon_i\cdot k_i)(k_i\cdot\epsilon_{3-i} )+(\epsilon_i\cdot\epsilon_{3-i})(k_i\cdot k_i)]\\
  &+4(\epsilon_{3-i}\cdot \epsilon_{3-i})
    [(\epsilon_i\cdot k_i)(k_i\cdot\epsilon_i )-(\epsilon_i\cdot k_i )(k_i\cdot\epsilon_i )+(\epsilon_i\cdot\epsilon_i )(k_i\cdot k_i)]\\
  &= 0.
\end{align*}

O traço em questão resulta em
\begin{align*}
  2\left[\frac{k_2\cdot p_-}{-k_1\cdot p_-}+\frac{-k_1\cdot p_-}{k_2\cdot
  p_-}+4(\epsilon_1\cdot\epsilon_1)^2-2\right].
\end{align*}
Basta agora substituir $k^0_2 = p^0_++p^0_--k^0_1$ e
$k_1=\frac{m^2+E_+m}{m+E_+-p_+\cos\theta}$ e temos a seção de choque
diferencial.
\begin{align}
  \frac{d\bar\sigma}{d\Omega_{k_1}}
  &=-\frac{\alpha^2(m+E_+)}{8p_+(m+E_+-p_+\cos\theta)^2} \left[\frac{k_2\cdot p_-}{-k_1\cdot p_-}+\frac{-k_1\cdot p_-}{k_2\cdot
    p_-}+4(\epsilon_1\cdot\epsilon_2)^2-2\right]\nonumber \\
  &=\frac{\alpha^2(m+E_+)}{8p_+(m+E_+-p_+\cos\theta)^2}\nonumber \\
  &\times\left[
    \frac{E_+-p_+\cos\theta}{m}+\frac m{E_+-p_+\cos\theta}+2-4(\epsilon_1\cdot\epsilon_2)^2
    \right]\label{eq:13}
\end{align}
Para a seção de choque total $\bar\sigma$, somamos $d\sigma/d\Omega_{k_1}$ sobre todas
as polarizações dos fótons finais e integramos sobre o ângulo sólido
$d\Omega_{k_1}$. Contudo, precisamos de cuidado com isso, uma vez que o
estado final tem duas partículas idênticas. A \autoref{eq:13} nos diz
que apenas um fóton emerge em $d\Omega_{k_1}$, e já que fótons são
indistinguíveis, pode ter sido qualquer um dos dois. Se integrarmos,
assim, $d\sigma/d\Omega_{k_1}$ sobre todo o ângulo sólido $4\pi$, contaríamos ambos
os fótons. Assim, precisamos dividir por dois a integral sobre o ângulo
sólido.
\begin{align*}
  \bar\sigma=\frac12\sum_{\lambda,\lambda'=1}^2\int d\Omega_{k_1}\frac{d\bar\sigma}{d\Omega_{k_1}}.
\end{align*}
Como calculado no efeito Compton,
\begin{align*}
  \frac12\sum_{\lambda,\lambda'=1}^2|\mathbf{\varepsilon}^{(\lambda)}\cdot\mathbf{\varepsilon}^{(\lambda')}|^2
  &=\frac12\cos^2\theta+1.
\end{align*}
Ou seja,
\begin{align*}
  \sum_{\lambda,\lambda'=1}^2\frac{d\bar\sigma}{d\Omega_{k_1}}
  &=\frac{\alpha^2(m+E_+)}{8p_+(m+E_+-p_+\cos\theta)^2}\nonumber \\
  &\times\left[
    4\frac{E_+-p_+\cos\theta}{m}+4\frac m{E_+-p_+\cos\theta}+8-4\cos^2\theta-8
    \right]\\
  &=\frac{\alpha^2(m+E_+)}{2p_+(m+E_+-p_+\cos\theta)^2}\nonumber \\
  &\times\left[
    \frac{E_+-p_+\cos\theta}{m}+\frac m{E_+-p_+\cos\theta}-\cos^2\theta
    \right].
\end{align*}
As aproximaçõe de alta e baixa energia para a seção de choque final são
obtidas facilmente, basta tomar $\mathbf{p}_+\to0$, $\mathbf{k_1\to-k_2}$ e
a média da polarização $(\epsilon_1\cdot\epsilon_2)^2\to\frac12$, temos
\begin{align*}
  \bar\sigma=\frac{\alpha^2\pi E_+}{p_+m^2}[1+\mathcal{O}(p_+^2/E_+^2)]
\end{align*}
com $p_+ << E_+$. No limite relativístico, no entanto,
\begin{align*}
  \bar\sigma=\frac{\alpha^2\pi}{mE_+}\left[\ln\frac{2E_+}m-1 +\mathcal{O}(\frac
  m{E_+}\ln\frac{E_+}{m})\right]  
\end{align*}
onde, da Equação 14, os primeiros dois termos contribuem igualmente e os
dois útlimos são menores por um fator de $m/E_+$.


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-master: t
%%% End:
